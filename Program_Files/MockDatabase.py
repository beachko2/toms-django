class MockDatabase:

    # Default constructor:
    def __init__(self):
        self.adminList = []
        self.professorList = []
        self.taList = []
        self.courseList = []
        self.labList = []


    def doesContainAdmin(self, adminEpanther):
        for currentAdmin in self.adminList:
            if (currentAdmin.userEpanther == adminEpanther):
                return True
        # Otherwise, we never found an Admin with the entered adminEpanther.
        return False


    def doesContainProfessor(self, professorEpanther):
        for currentProfessor in self.professorList:
            if (currentProfessor.userEpanther == professorEpanther):
                return True
        # Otherwise, we never found a Professor with the entered professorEpanther.
        return False


    def doesContainTA(self, taEpanther):
        for currentTA in self.taList:
            if (currentTA.userEpanther == taEpanther):
                return True
        # Otherwise, we never found a TA with the entered taEpanther.
        return False


    def doesContainCourse(self, newCourseCode, newCourseSection):
        for currentCourse in self.courseList:
            if currentCourse.courseCode == newCourseCode and currentCourse.courseSection == newCourseSection:
                return True
        # Otherwise, we never found a course with the entered course code and course section.
        return False


    def doesContainLab(self, courseCode, courseSection, labSection):
        return True


    def doesContainEpantherUsername(self, epanther):
        if (self.doesContainAdmin(epanther) or
            self.doesContainProfessor(epanther) or
            self.doesContainTA(epanther)):
            return True
        # Else, we never found the user with the given epanther.
        return False


    def findUser(self, epanther):
        searchForAdminResult = self.retrieveAdmin(epanther)
        if (searchForAdminResult != None):
            return searchForAdminResult
        searchForProfessorResult = self.retrieveProfessor(epanther)
        if (searchForProfessorResult != None):
            return searchForProfessorResult
        searchForTaResult = self.retrieveTA(epanther)
        if (searchForTaResult != None):
            return searchForTaResult
        # At this point, we never found the user with the entered epanther.
        return None


    def retrieveAdmin(self, epanther):
        for currentAdmin in self.adminList:
            if (currentAdmin.userEpanther == epanther):
                return currentAdmin
        # At this point, we never found an Admin with the given epanther.
        return None


    def retrieveProfessor(self, epanther):
        for currentProfessor in self.professorList:
            if (currentProfessor.userEpanther == epanther):
                return currentProfessor
        # At this point, we never found a professor with the given epanther.
        return None


    def retrieveTA(self, epanther):
        for currentTA in self.taList:
            if (currentTA.userEpanther == epanther):
                return currentTA
        # At this point, we never found a TA with the given epanther.
        return None


    def retrieveCourse(self, courseCode, courseSection):
        for currentCourse in self.courseList:
            if currentCourse.courseCode == courseCode and currentCourse.courseSection == courseSection:
                return currentCourse
        # Otherwise, we never found a course with the entered course code and course section.
        return None
