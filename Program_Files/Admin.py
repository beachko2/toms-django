from Program_Files.User import User
from Program_Files.ProcessCommand import ProcessCommand
from Program_Files.Professor import Professor
from Program_Files.TA import TA
from Program_Files.Course import Course
from Program_Files.Lab import Lab


class Admin(User):

    # Specifying constructor:
    def __init__(self, epanther, password, fullName, phoneNumber):
        self.userEpanther = epanther
        self.userPassword = password
        self.userFullName = fullName
        self.userPhoneNumber = phoneNumber


    @staticmethod
    def exitToMainMenu():
        ProcessCommand.displayMainMenu()

# End of class Admin.


class CreateUser:

    newUserEpanther = ""
    newUserPassword = ""
    newUserName = ""
    newUserPhone = ""
    newUserRole = ""
    newUserConfirm = ""


    @staticmethod
    def createUser():
        CreateUser._obtainNewUserEpanther()

        CreateUser.newUserPassword = input("Please enter a temporary password for user: ")
        CreateUser.newUserName = input("Please enter the user's full name (first, middle and last, separated by spaces): ")
        CreateUser.newUserPhone = input("Please enter the user's phone number (up to 10 digits, ex: 4145559999): ")

        CreateUser._obtainNewUserRole()


    @staticmethod
    def createProfessor():
        newProfessor = Professor(CreateUser.newUserEpanther, CreateUser.newUserPassword, CreateUser.newUserName,
                                 CreateUser.newUserPhone)

        if CreateUser.confirmNewUser():
            User.mockDatabase.professorList.append(newProfessor)
            print(CreateUser.newUserEpanther + " has been saved to system.\n")
        else:
            print(CreateUser.newUserEpanther + " has been discarded and not saved to system.\n")
        Admin.exitToMainMenu()


    @staticmethod
    def createTA():
        newTA = TA(CreateUser.newUserEpanther, CreateUser.newUserPassword, CreateUser.newUserName,
                   CreateUser.newUserPhone)

        if CreateUser.confirmNewUser():
            User.mockDatabase.taList.append(newTA)
            print(CreateUser.newUserEpanther + " has been saved to system.\n")
        else:
            print(CreateUser.newUserEpanther + " has been discarded and not saved to system.\n")
        Admin.exitToMainMenu()


    # Returns true if user should be saved, false if user should be discarded
    @staticmethod
    def confirmNewUser():
        print("\nNew user:")
        print("ePanther: " + CreateUser.newUserEpanther)
        print("Temporary password: " + CreateUser.newUserPassword)
        print("Full name: " + CreateUser.newUserName)
        print("Phone number: " + CreateUser.newUserPhone)
        print("Role: " + CreateUser.newUserRole + "\n")

        if CreateUser._obtainUserConfirmation():
            return True
        else:
            return False


    # Helper methods:


    # Continuously re-prompts for username until unique username is given
    @staticmethod
    def _obtainNewUserEpanther():
        CreateUser.newUserEpanther = input("\nPlease enter the user's ePanther ID: ")

        while User.mockDatabase.doesContainEpantherUsername(CreateUser.newUserEpanther):
            print("\nError, ePanther username " + CreateUser.newUserEpanther + " is already in the system.")
            CreateUser._obtainNewUserEpanther()


    # Continuously re-prompts for role until correct input is given (professor or ta)
    @staticmethod
    def _obtainNewUserRole():
        CreateUser.newUserRole = input("\n"
                                       "What role is this user?\n"
                                       "professor\n"
                                       "ta\n")

        if CreateUser.newUserRole == "professor":
            CreateUser.createProfessor()
        elif CreateUser.newUserRole == "ta":
            CreateUser.createTA()
        else:
            print("Invalid input.\n")
            CreateUser._obtainNewUserRole()


    # Returns true if user should be saved, false if user should be discarded
    @staticmethod
    def _obtainUserConfirmation():
        CreateUser.newUserConfirm = input("\n"
                                          "Add user?\n"
                                          "save\n"
                                          "discard\n")

        if CreateUser.newUserConfirm == "save":
            return True
        elif CreateUser.newUserConfirm == "discard":
            return False
        else:
            print("Invalid input\n")
            CreateUser._obtainUserConfirmation()

# End of class CreateUser.


class CreateCourse:

    newCourseCode = ""
    newCourseSectionNumber = ""
    newCourseName = ""
    newCourseDescription = ""
    newCourseNumberOfMeetingDays = 0
    newCourseMeetingDays = ""
    newCourseStartingHHMM = ""
    newCourseEndingHHMM = ""


    @staticmethod
    def createCourse():
        CreateCourse._gatherNewCourseInformation()
        CreateCourse._displayNewCourseInformation()
        CreateCourse._confirmNewCourseCreation()
        Admin.exitToMainMenu()


    # Private helper functions below:


    @staticmethod
    def _obtainMeetingDays(numberOfMeetingDays):
        newCourseMeetingDays = ""
        for currentMeetingDay in range (0, numberOfMeetingDays):
            newCourseMeetingDays += input("Please enter meeting day #" + str(currentMeetingDay + 1) + " (M T W R F S): ")
        return newCourseMeetingDays

    @staticmethod
    def _checkForDuplicateCourse():
        CreateCourse.newCourseCode = input("\nPlease enter the course code (ex. CS361): ")
        CreateCourse.newCourseSectionNumber = input("\nPlease enter the course section number: ")
        if User.mockDatabase.doesContainCourse(CreateCourse.newCourseCode, CreateCourse.newCourseSectionNumber):
            print("\nCourse already exists, please enter unique course code / course section.")
            CreateCourse._checkForDuplicateCourse()


    @staticmethod
    def _gatherNewCourseInformation():
        CreateCourse._checkForDuplicateCourse()
        CreateCourse.newCourseName = input("\nPlease enter the course name (ex. Data Structures & Algorithms): ")
        CreateCourse.newCourseDescription = input("\nPlease enter a description of the course (can be left blank): ")
        CreateCourse.newCourseNumberOfMeetingDays = int(input("\nPlease enter how many days a week this course will meet: "))
        CreateCourse.newCourseMeetingDays = CreateCourse._obtainMeetingDays(CreateCourse.newCourseNumberOfMeetingDays)
        CreateCourse.newCourseStartingHHMM = input \
            ("\nPlease enter the time this course will meet at (in military time, ex. 15:30 for 3:30 p.m.): ")
        CreateCourse.newCourseEndingHHMM = input \
            ("\nPlease enter the time the meetings in this course will end at (in military time): ")


    @staticmethod
    def _displayNewCourseInformation():
        print("\n"
              "New course:\n"
              "Course code: " + CreateCourse.newCourseCode + "\n"
              "Section number: " + CreateCourse.newCourseSectionNumber + "\n"
              "Name: " + CreateCourse.newCourseName + "\n"
              "Description: " + CreateCourse.newCourseDescription + "\n"
              "Class days: " + CreateCourse.newCourseMeetingDays + "\n"
              "Class start time: " + CreateCourse.newCourseStartingHHMM + "\n"
              "Class end time: " + CreateCourse.newCourseEndingHHMM + "\n")


    @staticmethod
    def _confirmNewCourseCreation():
        newCourseConfirmation = input("\n"
                                      "Add course?\n"
                                      "save\n"
                                      "discard\n")
        if newCourseConfirmation == "save":
            newCourseObject = CreateCourse._createNewCourseObject()
            User.mockDatabase.courseList.append(newCourseObject)
        elif newCourseConfirmation == "discard":
            Admin.exitToMainMenu()
        else:
            print("Invalid input\n")
            CreateCourse._confirmNewCourseCreation()


    @staticmethod
    def _createNewCourseObject():
        newCourseObject = Course()
        newCourseObject.courseCode = CreateCourse.newCourseCode
        newCourseObject.courseSection = CreateCourse.newCourseSectionNumber
        newCourseObject.courseName = CreateCourse.newCourseName
        newCourseObject.courseDescription = CreateCourse.newCourseDescription
        newCourseObject.meetingDays = CreateCourse.newCourseMeetingDays
        newCourseObject.startingHHMM = CreateCourse.newCourseStartingHHMM
        newCourseObject.endingHHMM = CreateCourse.newCourseEndingHHMM
        return newCourseObject


    @staticmethod
    def checkForCancel(userInput):
        if userInput == "cancel":
            Admin.exitToMainMenu()

# End of class CreateCourse.


class CreateLab:

    associatedCourseCode = ""
    associatedCourseSectionNumber = ""
    newLabSectionNumber = ""
    newLabNumberOfMeetingDays = 0
    newLabMeetingDays = ""
    newLabStartingHHMM = ""
    newLabEndingHHMM = ""


    @staticmethod
    def createLab():
        CreateLab._gatherNewLabInformation()
        CreateLab._displayNewLabInformation()
        CreateLab._confirmNewLabCreation()
        Admin.exitToMainMenu()


    # Private helper functions below:


    @staticmethod
    def _obtainMeetingDays(numberOfMeetingDays):
        newLabMeetingDays = ""
        for currentMeetingDay in range (0, numberOfMeetingDays):
            newLabMeetingDays += input("\nPlease enter meeting day #" + str(currentMeetingDay + 1) + " (M T W R F S): ")
        return newLabMeetingDays


    @staticmethod
    def _gatherNewLabInformation():
        CreateLab.associatedCourseCode = input("\nPlease enter the code of the associated course (ex. CS361): ")
        CreateLab.associatedCourseSectionNumber = input("\nPlease enter the section number of the associated course: ")
        CreateLab.newLabSectionNumber = input("\nPlease enter the lab section number (ex. 803 for section 803): ")
        CreateLab.newLabNumberOfMeetingDays = int(input("\nPlease enter how many days a week this lab will meet: "))
        CreateLab.newLabMeetingDays = CreateLab._obtainMeetingDays(CreateLab.newLabNumberOfMeetingDays)
        CreateLab.newLabStartingHHMM = input \
            ("\nPlease enter the time this lab will meet at (in military time, ex. 15:30 for 3:30 p.m.): ")
        CreateLab.newLabEndingHHMM = input \
            ("\nPlease enter the time the meetings in this lab will end at (in military time): ")


    @staticmethod
    def _displayNewLabInformation():
        print("\n"
              "New lab:\n"
              "Course code: " + CreateLab.associatedCourseCode + "\n"
              "Course section number: " + CreateLab.associatedCourseSectionNumber + "\n"
              "Lab section number: " + CreateLab.newLabSectionNumber + "\n"
              "Lab days: " + CreateLab.newLabMeetingDays + "\n"
              "Lab start time: " + CreateLab.newLabStartingHHMM + "\n"
              "Lab end time: " + CreateLab.newLabEndingHHMM + "\n")


    @staticmethod
    def _confirmNewLabCreation():
        newLabConfirmation = input("\n"
                                   "Add lab?\n"
                                   "save\n"
                                   "discard\n")
        if newLabConfirmation == "save":
            newLabObject = CreateLab._createNewLabObject()
            User.mockDatabase.labList.append(newLabObject)
        elif newLabConfirmation == "discard":
            Admin.exitToMainMenu()
        else:
            print("Invalid input\n")
            CreateLab._confirmNewLabCreation()


    @staticmethod
    def _createNewLabObject():
        newLabObject = Lab()
        newLabObject.courseCode = CreateLab.associatedCourseCode
        newLabObject.courseSection = CreateLab.associatedCourseSectionNumber
        newLabObject.labSection = CreateLab.newLabSectionNumber
        newLabObject.meetingDays = CreateLab.newLabMeetingDays
        newLabObject.startingHHMM = CreateLab.newLabStartingHHMM
        newLabObject.endingHHMM = CreateLab.newLabEndingHHMM


    @staticmethod
    def checkForCancel(userInput):
        if userInput == "cancel":
            Admin.exitToMainMenu()

# End of class CreateLab.


class AssignToCourse:

    enteredEpantherId = ""
    enteredCourseCode = ""
    enteredCourseSection = ""
    courseToAssignTo = None


    @staticmethod
    def assignProfessorToCourse():
        _AssignProfessorToCourse._obtainProfessorEpantherId()
        AssignToCourse._obtainCourseToAssignTo()
        _AssignProfessorToCourse._checkIfCourseAlreadyHasProfessor()
        _AssignProfessorToCourse._confirmProfessorToCourseAssignment()


    @staticmethod
    def assignTaToCourse():
        _AssignTaToCourse._obtainTaEpantherId()
        AssignToCourse._obtainCourseToAssignTo()
        _AssignTaToCourse._confirmTaToCourseAssignment()


    # Private helper/implementation functions below:


    @staticmethod
    def _obtainCourseToAssignTo():
        AssignToCourse.enteredCourseCode = input("\nPlease enter the course code (ex. CS361): ")
        AssignToCourse.enteredCourseSection = input("\nPlease enter the course section number: ")
        AssignToCourse._verifyCourseAndSectionExist()
        AssignToCourse.courseToAssignTo = User.mockDatabase.retrieveCourse \
            (AssignToCourse.enteredCourseCode, AssignToCourse.enteredCourseSection)


    @staticmethod
    def _verifyCourseAndSectionExist():
        if not User.mockDatabase.doesContainCourse(AssignToCourse.enteredCourseCode, AssignToCourse.enteredCourseSection):
            print("\nError, no course was found with course code " + AssignToCourse.enteredCourseCode +
                  " and section number " + AssignToCourse.enteredCourseSection + ".")
            AssignToCourse._obtainCourseToAssignTo()
        # Else, we found the course.

# End of class AssignCourse.


# Below are private classes, which are full of private helper methods that are used in the above AssignToCourse class.


class _AssignProfessorToCourse:

    @staticmethod
    def _obtainProfessorEpantherId():
        AssignToCourse.enteredEpantherId = input("\nPlease enter the professor’s ePanther ID: ")
        _AssignProfessorToCourse._verifyEpantherIdIsProfessor()


    @staticmethod
    def _verifyEpantherIdIsProfessor():
        if not User.mockDatabase.doesContainProfessor(AssignToCourse.enteredEpantherId):
            print("\nError, no professor with the username " + AssignToCourse.enteredEpantherId + " found.")
            _AssignProfessorToCourse._obtainProfessorEpantherId()


    @staticmethod
    def _checkIfCourseAlreadyHasProfessor():
        if AssignToCourse.courseToAssignTo.doesHaveProfessor():
            print("\nError, course " + AssignToCourse.enteredCourseCode + "-" + AssignToCourse.enteredCourseSection +
                  " already has professor " + AssignToCourse.enteredEpantherId + " assigned to it.\n"
                  "Continuing with this assignment will overwrite the current professor for this course.")


    @staticmethod
    def _confirmProfessorToCourseAssignment():
        assignToCourseConfirmation = input("\nAssign " + AssignToCourse.enteredEpantherId + " to " +
                                           AssignToCourse.enteredCourseCode + "-" + AssignToCourse.enteredCourseSection + "?\n"
                                           "yes\n"
                                           "no\n")
        if assignToCourseConfirmation == "yes":
            AssignToCourse.courseToAssignTo.professorEpanther = AssignToCourse.enteredEpantherId
            Admin.exitToMainMenu()
        elif assignToCourseConfirmation == "no":
            Admin.exitToMainMenu()
        else:
            print("Invalid input\n")
            _AssignProfessorToCourse._confirmProfessorToCourseAssignment()

# End of class _AssignProfessorToCourse.


class _AssignTaToCourse:

    @staticmethod
    def _obtainTaEpantherId():
        AssignToCourse.enteredEpantherId = input("\nPlease enter the TA’s ePanther ID: ")
        _AssignTaToCourse._verifyEpantherIdIsTa()


    @staticmethod
    def _verifyEpantherIdIsTa():
        if not User.mockDatabase.doesContainTA(AssignToCourse.enteredEpantherId):
            print("\nError, no TA with the username " + AssignToCourse.enteredEpantherId + " found.")
            _AssignTaToCourse._obtainTaEpantherId()


    @staticmethod
    def _confirmTaToCourseAssignment():
        assignToCourseConfirmation = input("\nAssign " + AssignToCourse.enteredEpantherId + " to " +
                                           AssignToCourse.enteredCourseCode + "-" + AssignToCourse.enteredCourseSection + "?\n"
                                           "yes\n"
                                           "no\n")
        if assignToCourseConfirmation == "yes":
            AssignToCourse.courseToAssignTo.taEpantherList.append(AssignToCourse.enteredEpantherId)
            Admin.exitToMainMenu()
        elif assignToCourseConfirmation == "no":
            Admin.exitToMainMenu()
        else:
            print("Invalid input\n")
            _AssignTaToCourse._confirmTaToCourseAssignment()

# End of class _AssignTaToCourse.
