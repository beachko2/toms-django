import Program_Files.User
import Program_Files.Admin


class ProcessCommand:
    userInput = ""
    listOfValidCommands = ["login", "logout", "assign", "create", "course", "lab", "professor", "ta", "user", "cancel"]


    @staticmethod
    def processCommand():
        ProcessCommand._checkIfValidCommand()
        ProcessCommand._executeCommand()


    @staticmethod
    def displayMainMenu():
        ProcessCommand.userInput = input("\n"
                                         "What would you like to do?\n"
                                         "assign\n"
                                         "create\n"
                                         "logout\n")
        ProcessCommand.processCommand()


    @staticmethod
    def displayCreateMenu():
        ProcessCommand.userInput = input("\n"
                                         "What would you like to create?\n"
                                         "course\n"
                                         "lab\n"
                                         "user\n"
                                         "cancel\n")
        ProcessCommand.processCommand()


    @staticmethod
    def displayAssignMenu():
        ProcessCommand.userInput = input("\n"
                                         "Who would you like to assign?\n"
                                         "professor\n"
                                         "ta\n"
                                         "cancel\n")
        ProcessCommand.processCommand()


    # Private helper methods/functions below.


    @staticmethod
    def _checkIfValidCommand():
        if (not ProcessCommand.userInput in ProcessCommand.listOfValidCommands):
            print("\n" + "Error, that is not a valid command.")
            ProcessCommand.displayMainMenu()
        # Otherwise, they entered a proper command.


    @staticmethod
    def _executeCommand():
        if ProcessCommand.userInput == "logout":
            Program_Files.User.User.logout()
        elif ProcessCommand.userInput == "assign":
            ProcessCommand.displayAssignMenu()
        elif ProcessCommand.userInput == "create":
            ProcessCommand.displayCreateMenu()
        elif ProcessCommand.userInput == "course":
            Program_Files.Admin.CreateCourse.createCourse()
        elif ProcessCommand.userInput == "lab":
            Program_Files.Admin.CreateLab.createLab()
        elif ProcessCommand.userInput == "professor":
            Program_Files.Admin.AssignToCourse.assignProfessorToCourse()
        elif ProcessCommand.userInput == "ta":
            Program_Files.Admin.AssignToCourse.assignTaToCourse()
        elif ProcessCommand.userInput == "user":
            Program_Files.Admin.CreateUser.createUser()
        elif ProcessCommand.userInput == "cancel":
            ProcessCommand.displayMainMenu()
        else:
            print("Command unsupported at this time.")

# End of class ProcessCommand.
