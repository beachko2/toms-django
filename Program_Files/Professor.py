from Program_Files.User import User

class Professor(User):

    # No additional class fields.

    # Specifying constructor:
    def __init__(self, epanther, password, fullName, phoneNumber):
        self.userEpanther = epanther
        self.userPassword = password
        self.userFullName = fullName
        self.userPhoneNumber = phoneNumber
        self.courseAssignments = []
