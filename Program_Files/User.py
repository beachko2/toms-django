from abc import ABC
from Program_Files.ProcessCommand import ProcessCommand

#Abstract Base Class (ABC).
class User(ABC):

    mockDatabase = None
    enteredEpanther = ""
    enteredPassword = ""


    @staticmethod
    def passInMockDatabase(mockDatabase):
        User.mockDatabase = mockDatabase


    @staticmethod
    def login():
        User._obtainEpanther()
        User._obtainPassword()
        ProcessCommand.displayMainMenu()


    @staticmethod
    def logout():
        print("\n" + User.enteredEpanther + " has successfully logged out.")
        User.login()


    # Private helper methods/functions below.
    # Python apparently doesn't have private functions, but the Python documentation recommends prefixing each
    # function with an _underscore to identify it as one that should NOT be used outside of the class it is declared in.


    @staticmethod
    def _obtainEpanther():
        User._promptForEpanther()
        User._checkIfEpantherExists()


    @staticmethod
    def _promptForEpanther():
        User.enteredEpanther = input("\n" + "Please enter your ePanther username: ")


    @staticmethod
    def _checkIfEpantherExists():
        if not User.mockDatabase.doesContainEpantherUsername(User.enteredEpanther):
            print("\n" + "Error, username " + User.enteredEpanther + " does not exist.")
            User._obtainEpanther()
        # Else we found their ePanther.


    @staticmethod
    def _obtainPassword():
        User._promptForPassword()
        User._checkIfPasswordMatchesEpanther()


    @staticmethod
    def _promptForPassword():
        User.enteredPassword = input("\n" + "Please enter your password: ")


    @staticmethod
    def _checkIfPasswordMatchesEpanther():
        userAccount = User.mockDatabase.findUser(User.enteredEpanther)
        if (userAccount.userPassword != User.enteredPassword):
            print("\n" + "Error, incorrect password entered.")
            User._obtainPassword()
        # Else their password is legit.
