from Program_Files.MockDatabase import MockDatabase
from Program_Files.Admin import Admin
from Program_Files.Professor import Professor
from Program_Files.TA import TA
from Program_Files.Course import Course
from Program_Files.User import User


# We can run this file to launch the program.
class RunProgram:

    # Set up MockDatabase:
    mockDatabase = MockDatabase()

    # Add admin, professor, and TA accounts to it:
    mockDatabase.adminList.append( Admin("boyland", "password", "John Boyland", 4149999999) )
    mockDatabase.professorList.append( Professor("rock", "password", "Jayson Rock", 4149999999) )
    mockDatabase.taList.append( TA("prasada", "password", "Apoorv Prasad", 4149999999) )

    # Add a course to it:
    newCourseToAdd = Course()
    newCourseToAdd.courseCode = "CS361"
    newCourseToAdd.courseSection = "001"
    mockDatabase.courseList.append(newCourseToAdd)

    # Start login sequence:
    User.passInMockDatabase(mockDatabase)
    User.login()
