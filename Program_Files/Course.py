
class Course:

    def __init__(self):
        self.courseCode = ""
        self.courseSection = 0
        self.courseName = ""
        self.courseDescription = ""
        self.meetingDays = []
        self.startingHHMM = ""
        self.endingHHMM = ""
        self.hasLab = False # Will remain false until a new lab is created and assigned to this course.
        self.professorEpanther = None
        self.taEpantherList = []


    def doesHaveProfessor(self):
        return self.professorEpanther != None
