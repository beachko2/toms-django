import unittest
from Program_Files.Admin import Admin
from Program_Files.CourseTime import CourseTime


class Test_CourseTest(unittest.TestCase):
    def test_init_00(self):
        self.assertEqual(self.courseTime1, str)
        self.assertEqual(self.courseTime2, str)

    def test_init_01(self):
        with self.assertRaises(ValueError) as context:
            self.courseTime8

    def test_init_02(self):
        with self.assertRaises(TypeError) as context:
            self.courseTime7

    def test_str_00(self):

    def test_start_00(self):
        self.AssertEquals(self.courseTime1, 1200);

    def test_start_01(self):
        self.AssertEquals(self.courseTime1, 1200)

    def test_start_02(self):
        self.AssertEquals(self.courseTime4, 1200)

    def test_start_03(self):
        self.AssertFalse(self.courseTime4, 1500)

    def test_start_04(self):
        self.AssertFalse(self.courseTime5, 2500)

    def test_end_01(self):
        self.AssertEquals(self.courseTime1, 1300);

    def test_end_02(self):
        self.AssertEquals(self.courseTime1, 1300)

    def test_end_00(self):
        self.AssertFalse(self.courseTime1, 1300)

    def test_end_03(self):
        self.AssertEquals(self.courseTime3, 1300)

    def test_end_04(self):
        self.AssertFalse(self.courseTime4, 1200)

    def test_end_05(self):
        self.AssertEquals(self.courseTime5, 1000)


    def test_isOnline_00(self):
        self.assertFalse(self.courseTime1.isOnline)

    def test_isOnline_01(self):
        self.assertTrue(self.courseTime2.isOnline)

    def test_isOnline_02(self):
        self.assertFalse(self.courseTime3.isOnline)

    def test_isOnline_03(self):
        self.assertFalse(self.courseTime4.isOnline)

    def test_isOnline_04(self):
        self.assertFalse(self.courseTime5.isOnline)

    def test_isOverlap_00(self):


    def setUp(self):
        self.courseTime1 = CourseTime("1200 1300 MW")   #correct input

        self.courseTime2 = CourseTime("Online")         #test online

        self.courseTime3 = CourseTime("1200 1300 Z")    #wrong day

        self.courseTime4 = CourseTime("1200 1300 MWF")  #conflicting time on M & W

        self.courseTime5 = CourseTime("2500 1000 MF")   #time out of bounds

        self.courseTime6 = CourseTime("1000 1200 T")    # time out of bounds

        self.courseTime7 = CourseTime(500)              # Wrong input

        self.courseTime8 = CourseTime("1000 2000 3000 MW") #Incorrect input

    ## isOverlap TESTS ##

    # verify that an online course does not overlap with a non-online course
    def test_isOverlap_00(self):
        self.assertFalse( self.courseTime2.isOverlap(self.courseTime1) )

    # Verify that two online courses do not conflict with each other
    def test_isOverlap_01(self):
        self.assertFalse( self.courseTime2.isOverlap(self.courseTime2) )

    # Verify that two non-online courses conflict with one another
    def test_isOverlap_02(self):
        self.assertTrue( self.courseTime1.isOverlap(self.courseTime4) )

    # Verify that two non-online courses do not conflict with each other
    def test_isOverlap_03(self):
        unittest.assertFalse( self.courseTime1.isOverlap(self.courseTime6) )
        self.assertFalse( self.courseTime1.isOverlap(self.courseTime6) )


    ## __str__ TESTS ##

    # Verify the correct output for a non-online class
    def test___str__(self):
        self.assertEquals( "Course start to end times: 1200 to 1300, Course days: MW", self.courseTime1.__str__() )

    # Verify the correct output for an online class
    def test___str__(self):
        self.assertEquals( "Course start to end times: ONLINE, Course days: ONLINE", self.courseTime1.__str__() )