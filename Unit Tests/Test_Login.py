import unittest
from unittest.mock import patch

from Program_Files.Admin import Admin
from Program_Files.User import User
from Program_Files.MockDatabase import MockDatabase

class Test_User(unittest.TestCase):
    def setUp(self):
        self.database = MockDatabase()
        User.mockDatabase = self.database
        self.boyland = Admin("boyland", "password", "John Boyland", 4149999999)
        self.database.adminList.append(self.boyland)


    # Public functions like login and logout seem to be more suited for acceptance tests, where multiple
    # inputs are entered in.
    # As such, I think we can just stick to testing private functions for now, which actually are testable.

    # So, in order to simulate user input, we can use @patch to "patch through" input when asked for.
    # Inside _promptForEpanther is the line of code: User.enteredEpanther = input("Please enter your ePanther username: ")
    # When the test reaches that line of code, the value "boyland" is passed into input, as if it were typed by a person.
    @patch('builtins.input', return_value='boyland')
    def test_promptForEpanther(self, input): # Don't remove the input argument - @patch uses this.
        User._promptForEpanther()
        self.assertEqual("boyland", User.enteredEpanther)
        print(User.enteredEpanther)


    @patch('builtins.input', return_value='password')
    def test_promptForPassword(self, input):
        User._promptForPassword()
        self.assertEqual("password", User.enteredPassword)
        print(User.enteredPassword)
