import unittest
from Roles import Admin


class TestAdmin(unittest.TestCase):
    def setUp(self):
        self.prof1 = Admin(
            'boyland',
            'boyland@uwm.edu',
            '987654321',
            'password1')

    # We are gonna assume that we have following Models interacting with our Application right now:

    # <MODEL 1> Admin Details:
    #     name         => 'boyland',
    #     email        => 'boyland@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 2> Professor Details:
    #     name         => 'rock',
    #     email        => 'rock@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 3> TA Details:
    #     name         => 'apoorv',
    #     email        => 'prasada@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 4> Lab Details:
    #     name         => 'CS361',
    #     time         => '11:30am - 12:45pm'
    #     section      => '801'

    # <MODEL 5> Course Details:
    #     name         => 'CS361',
    #     description  => 'Intro to Software Engineering',
    #     times        => '12pm-1pm'
    #     section      => '401'
    #     lab          => 'true'

    def test_login_admin(self):
        # Admin can log in with the correct credentials (1)
        # User Story 1
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEqual(self.ui.command("What would you like to do? create, logout"))

    def test_invalid_login_admin(self):
        # Admin is prompted to reenter
        # password when they enter the wrong password
        # User Story 1
        self.assertEquals(self.ui.command("Login boyland passqword1"), "Invalid Password, try again")
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEqual(self.ui.command("What would you like to do? create, logout"))

    def test_help_admin(self):
        self.ui.command("Login boyland password1")
        # all of these commands should be available to an admin
        self.assertEquals(self.ui.command("help"), "Commands:"
                                                   "Login"
                                                   "Edit"
                                                   "Delete"
                                                   "Create"
                                                   "Assign"
                                                   "View" 
                                                   "Send")

    def test_create_course_admin(self):
        # User Story: 3
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Create"),
                         "What would you like to create?: course, lab, user, cancel")
        self.assertEqual(self.ui.command("course"), "Please enter the course code (ex. CS361): ")
        self.assertEqual(self.ui.command("CS351"), "Course created successfully. Please enter the section number: ")
        self.assertEqual(self.ui.command("801"), "Please enter the course name: ")
        self.assertEqual(self.ui.command("Data Structures and Algorithms"), "Please enter a description of the course (can be left blank): ")
        self.assertEqual(self.ui.command("This is a course about data structures and algorithms. And it's really hard."),"Please enter how many days a week this course will meet: ")
        self.assertEqual(self.ui.command("2"), "Enter meeting day 1: ")
        self.assertEqual(self.ui.command("M"), "Enter meeting day 2: ")
        self.assertEqual(self.ui.command("W"), "Please enter the time this course will meet (in military time): ")
        self.assertEqual(self.ui.command("15:00"), "Please enter the time this course will end (in military time): ")
        self.assertEqual(self.ui.command("15:50"), "New course:"
                                                   "Course code: CS351 "
                                                    "Section number: 801"
                                                    "Course name: Data Structures and Algorithsm"
                                                    "Description: This is a course about data structures and algorithms. And it's really hard."
                                                    "Class Days: MW" 
                                                    "Class start: 15:00"
                                                    "Class end: 15:50"
                                                    "--> save or discard")
        self.assertEqual(self.ui.command("save"), "New course has been saved.")

    def test_create_duplicate_course_admin(self):
        # User Story: 3
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Create CS361"), "Course already exists")
        self.assertEqual(self.ui.command("Create CS251"), "Course created successfully.")

    def test_invalid_course_admin(self):
        # User Story: 3
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEqual(self.ui.command("Create CS9667"), "INVALID COURSE")
        self.assertEqual(self.ui.command("Create MATH9667"), "INVALID COURSE")
        self.assertEqual(self.ui.command("Create Hello"), "INVALID COURSE")

    def test_create_account_admin(self):
        #User Story: 2
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Create"),
                         "What would you like to create?: course, lab, user, cancel")

        self.assertEqual(self.ui.command("user"), "Please enter the user's ePanther ID:")

    def test_invalid_prompt_create_account_admin(self):
        #User Story: 2
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Create"),
                         "What would you like to create?: course, lab, user, cancel")
        self.assertEqual(self.ui.command("schedule"), "Invalid command. What would you like to create?: course, lab, user, cancel")

        self.assertEqual(self.ui.command("user"), "Please enter the user's ePanther ID:")

    def test_create_account_TA_admin(self):
        #User Story: 2
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Create"),
                         "What would you like to create?: course, lab, user, cancel")
        self.assertEqual(self.ui.command("user"), "Please enter the user's ePanther ID:")

        self.assertEqual(self.ui.command("apoorv"), "Account for apoorv successfully created. Please enter a temporary password for the user: ")
        self.assertEqual(self.ui.command("tempPass"), "Please enter the user's full name: ")
        self.assertEqual(self.ui.command("Apoorv Prasada", "Please enter the user's phone number (format: AAAXXXXXXX): "))
        self.assertEqual(self.ui.command("4141112222"), "What role is this user? professor, ta")
        self.assertEqual(self.ui.command("ta"), "New User: "
                                                "ePanther: apoorv"
                                                "Temporary password: tempPass"
                                                "Full name: Apoorv Prasada"
                                                "Phone number: 4141112222 "
                                                "Role: ta"
                                                "--> save or discard")
        self.assertEqual("save", "New account saved.")

    def test_create_account_prof_admin(self):
        # User Story: 2
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Create"),
                         "What would you like to create?: course, lab, user, cancel")
        self.assertEqual(self.ui.command("user"), "Please enter the user's ePanther ID:")

        self.assertEqual(self.ui.command("rock"),
                         "Account for rock successfully created. Please enter a temporary password for the user: ")
        self.assertEqual(self.ui.command("tempPass"), "Please enter the user's full name: ")
        self.assertEqual(
            self.ui.command("Jayson Rock", "Please enter the user's phone number (format: AAAXXXXXXX): "))
        self.assertEqual(self.ui.command("4143334444"), "What role is this user? professor, ta")
        self.assertEqual(self.ui.command("professor"), "New User: "
                                                "ePanther: rock"
                                                "Temporary password: tempPass"
                                                "Full name: Jayson Rock"
                                                "Phone number: 4143334444 "
                                                "Role: professor"
                                                "--> save or discard")
        self.assertEqual("save", "New account saved.")

    def test_create_duplicate_account_admin(self):
        # User Story: 2
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Create"),
                         "What would you like to create?: course, lab, user, cancel")
        self.assertEqual(self.ui.command("user"), "Please enter the user's ePanther ID:")

        self.assertEqual(self.ui.command("rock"), "Duplicate account. Account not created.")

    def test_delete_account_TA_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Delete apoorv"),
                         "Account for apoorv deleted successfully")

    def test_delete_account_prof_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Delete rock"),
                         "Account for rock deleted successfully")

    def test_delete_invalid_account_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Delete asdfjkl"),
                         "Invalid account.")

    # edit their own contact information (not course assignments)
    def test_edit_information_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit boyland",
                                          "Choose Information to edit: Username, Password, Schedule, Phone: "))

    def test_edit_account_TA_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit",
                                          "What would you like to edit? course, lab, user, cancel"))
        self.assertEqual(self.ui.command("user"), "Please enter the user's ePanther ID")
        self.assertEqual(self.ui.command("apoorv"), "")

    def test_edit_account_prof_admin(self):
        self.assertEquals(self.ui.command("Login boyland@uwm.edu password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit rock",
                                          "Choose Information to edit: Username, Password, Schedule, Phone: "))

    def test_send_out_notification_all_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Send ALL <message>."), "<message> sent successfully to all.")

    def test_assign_professor_to_courses_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Assign rock CS361"), "Jason Rock has been assigned to CS361")

    def test_assign_ta_to_courses_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Assign apoorv CS361"), "Apoorv has been assigned to CS361")

    def test_assigning_ta_to_lab_admin(self):
        self.assertEquals(self.ui.command("Login boyland password1"), "Access Granted")

        self.assertEqual(self.ui.command("Assign apoorv CS361"), "Apoorv has been assigned to CS361")
        self.assertEqual(self.ui.command("Assign apoorv to CS361 Section 801"),
                         "Apoorv has been assigned to CS361 Section 801")

    # send out notifications to their TAs via UWM email
    def test_EmailSpecificTA_admin(self):
        self.assertEquals(self.ui.command("Send apoorv <message>"),
                          "Email was successfully sent to apoorv.")
        self.assertEquals(self.ui.command("Send shudbfg <message>"),
                          "Username doesn't exist.")

    # read public contact information of all users
    def test_ReadAllUsersContactInformation_admin(self):
        self.assertEquals(self.ui.command("View <ALL>"),
                          "Name: Apoorv Lastname"
                          "Role: TA"
                          "Email: prasada@uwm.edu"
                          "Phone: 9999999919")
