import unittest
from Roles import TA


class TestTA(unittest.TestCase):
    def setUp(self):
        self.ta1 = TA(
            'apoorv',
            'employee1@company.com',
            '987654321',
            'password1', )

    # We are gonna assume that we have following Models interacting with our Application right now:

    # <MODEL 1> Admin Details:
    #     name         => 'boyland',
    #     email        => 'boyland@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 2> Professor Details:
    #     name         => 'rock',
    #     email        => 'rock@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 3> TA Details:
    #     name         => 'apoorv',
    #     email        => 'prasada@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 4> Lab Details:
    #     name         => 'CS361',
    #     time         => '11:30am - 12:45pm'
    #     section      => '801'

    # <MODEL 5> Course Details:
    #     name         => 'CS361',
    #     description  => 'Intro to Software Engineering',
    #     times        => '12pm-1pm'
    #     section      => '401'
    #     lab          => 'true'

    def test_login_ta(self):
        # TA can log in with correct credentials
        self.assertEquals(self.ui.command("Login prasada password1"), "Access Granted")

    def test_invalid_login_ta(self):
        # TA is prompted to reenter
        # password when they enter the wrong password
        self.assertEquals(self.ui.command("Login prasada passqword1"), "Invalid Password, try again")
        self.assertEquals(self.ui.command("Login prasada password1"), "Access Granted")

    def test_help_ta(self):
        self.ui.command("Login prasada@uwm.edu password1")
        # only these commands should be available to a TA
        self.assertEquals(self.ui.command("help"), "Commands:"
                                                   "Login"
                                                   "Edit"
                                                   "View"
                                                   "Send")

    # edit their own contact information (not course assignments)
    def test_edit_information_ta(self):
        self.assertEquals(self.ui.command("Login prasada password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit prasada"),
                              "Choose Information to edit: Password, Schedule, Phone: ")
        self.assertEquals(self.ui.command("Password"), "Enter new password")
        self.assertEquals(self.ui.command("newpassword1"), "Password set")

        self.assertEquals(self.ui.command("Phone"), "Enter new phone number")
        self.assertEquals(self.ui.command("4140002330"), "phone number set")

    # TAs cannot view course assignments
    def test_view_course_assignments_ta(self):
        self.assertEquals(self.ui.command("Login prasada password1"), "Access Granted")

        self.assertEquals(self.ui.command("View course assignments"), "You do not have course assignment permissions")

    # TAs cannot view TA assignments (for all TAs)
    def test_view_ta_assignments_ta(self):
        self.assertEquals(self.ui.command("Login prasada password1"), "Access Granted")

        self.assertEquals(self.ui.command("View TA assignments"), "You do not have course assignment permissions")

    # TAs cannot create courses
    def test_create_course_ta(self):
        self.assertEquals(self.ui.command("Login prasada password1"), "Access Granted")

        self.assertEquals(self.ui.command("Create <CS351>"), "You do not have class creation permissions")

    # send out notifications to their TAs via UWM email
    def test_EmailSpecificTA_prof(self):
        self.assertEquals(self.ui.command("Send rock <message>"),
                            "Email was successfully sent to TA@uwm.edu")
        self.assertEquals(self.ui.command("Send shudbfg <message>"),
                            "Username shudbfg doesn't exist")

    # Ta cannot send to all users
    def test_ReadAllUsersContactInformation_ta(self):
        self.assertEquals(self.ui.command("Send <ALL> message"),
                          "You do not have send all permissions")

    # read public contact information of all users
    def test_ReadAllUsersContactInformation_ta(self):
        self.assertEquals(self.ui.command("View <ALL>:"),
                          "Name: Apoorv Prasad"
                          "Role: TA"
                          "Email: prasada@uwm.edu"
                          "Phone: 9999999919")
