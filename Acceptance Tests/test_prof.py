import unittest
from Roles import Professor


class TestProfessor(unittest.TestCase):
    def setUp(self):
        self.prof1 = Professor(
            'rock',
            'rock@uwm.edu',
            '987654321',
            'password1', )

    # We are gonna assume that we have following Models interacting with our Application right now:

    # <MODEL 1> Admin Details:
    #     name         => 'boyland',
    #     email        => 'boyland@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 2> Professor Details:
    #     name         => 'rock',
    #     email        => 'rock@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 3> TA Details:
    #     name         => 'prasada',
    #     email        => 'prasada@uwm.edu',
    #     phone        => '987654321',
    #     password     => 'password1',

    # <MODEL 4> Lab Details:
    #     name         => 'CS361',
    #     time         => '11:30am - 12:45pm'
    #     section      => '801'

    # <MODEL 5> Course Details:
    #     name         => 'CS361',
    #     description  => 'Intro to Software Engineering',
    #     times        => '12pm-1pm'
    #     section      => '401'
    #     lab          => 'true'

    # Login Tests
    def test_login_prof(self):
        # Prof can log in with correct credentials
        self.assertEquals(self.ui.command("Login rock password1"), "Access Granted")

    def test_invalid_login_prof(self):
        # Prof is prompted to reenter
        # password when they enter the wrong password
        self.assertEquals(self.ui.command("Login rock passqword1"), "Invalid Password, try again")
        self.assertEquals(self.ui.command("Login rock password1"), "Access Granted")

    def test_help_prof(self):
        self.assertEquals(self.ui.command("Login rock password1"), "Access Granted")
        # only these commands should be available to a prof
        self.assertEquals(self.ui.command("help"), "Commands:"
                                                   "Login"
                                                   "Edit"
                                                   "Assign"
                                                   "View")

    # edit their own contact information (not course assignments)
    def test_edit_information_prof(self):
        self.assertEquals(self.ui.command("Login rock password1"), "Access Granted")
        self.assertEquals(self.ui.command("Edit rock"),
                          "Choose Information to edit: Username, Password, Schedule, Phone: ")

    # view course assignments
    def test_view_course_assignments_prof(self):
        self.assertEquals(self.ui.command("Login rock password1"), "Access Granted")
        self.assertEquals(self.ui.command("View assignments CS361"), "TA apoorv, Prof rock")

    # view TA assignments (for all TAs)
    def test_TA_assignments_prof(self):
        self.assertEquals(self.ui.command("Login rock password1"), "Access Granted")

        self.assertEquals(self.ui.command("TA assignments"), "One assignment found")

    # send out notifications to their TAs via UWM email
    def test_MessageSpecificTA_prof(self):
        self.assertEquals(self.ui.command("Send prasada <message>"),
                          "Email was successfully sent to prasada.")
        self.assertEquals(self.ui.command("Send shudbfg <message>"),
                          "Username shudbfg doesn't exist.")

    # professors cannot assign other professors
    def test_assign_professor_to_courses_prof(self):
        self.ui.command("Login rock password1"), "Access Granted"

        self.assertEqual(self.ui.command("Assign rds <CS361>"),
                         "You do not have permission to add professors")

    # professors can assign TAs
    def test_assign_ta_to_courses_prof(self):
        self.ui.command("Login rock password1"), "Access Granted"

        self.assertEqual(self.ui.command("Assign apoorv <CS361>"), "apoorv has been assigned to <CS361>")
        self.assertEqual(self.ui.command("Assign apoorv <CS361>"), "bob has been assigned to <CS361>")

    # read public contact information of all users
    def test_ReadAllUsersContactInformation_prof(self):
        self.assertEquals(self.ui.command("View <ALL>"),
                          "Name: Apoorv Prasad"
                          "Role: TA"
                          "Email: prasada@uwm.edu"
                          "Phone: (414) 999-9999")

    # read public contact information of a users
    def test_ReadUsersContactInformation_prof(self):
        self.assertEquals(self.ui.command("View <apoorv>"),
                          "Name: Apoorv Prasad"
                          "Role: TA"
                          "Email: prasada@uwm.edu"
                          "Phone: (414) 999-9999")

#
