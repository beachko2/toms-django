import unittest
from ScrumManager import ScrumManager
from Developer import Developer

class ScrumManagerToDeveloperTestCase(unittest.TestCase):
    def setUp(self):
        self.scrumManager1 = ScrumManager('12345', 'scrum hero',
                                          'employee1@company.com',
                                          '987654321',
                                          'password1',
                                          'Scrum Manager')

        self.developer1 = Developer('67891', 'developer bro',
                                    'employee2@company.com',
                                    '12345678',
                                    'password2',
                                    'Developer')

    # We are gonna assume that we have following Models interacting with our Application right now:

    # <MODEL 1> Scrum Manager Details:
    #     id           => '12345',
    #     name         => 'scrum hero',
    #     email        => 'employee1@company.com',
    #     phone        =>  '987654321',
    #     password     => 'password1',
    #     role         => 'Scrum Manager'

    # <MODEL 2> Developer Details:
    #     id           => '67891',
    #     name         => 'developer bro',
    #     email        => 'employee2@company.com',
    #     phone        => '12345678',
    #     password     => 'password2',
    #     role         => 'Developer'

    # <MODEL 3> Task Details:
    #     task_id                  => '12',
    #     task_description         => 'Update the Terms Policy',
    #     task_estimate            => 'Medium',



    def test_valid_developer(self):
        # User Story : .....
        self.ui.command("Login employee1@company.com password1")
        self.assertEqual(self.ui.command("Access Developer 67891"), ' Name: developer bro  Email: employee2@company.com' 
                                                                    'Phone: 12345678')



    def test_invalid_developer(self):
        # User Story : .....
        self.ui.command("Login employee1@company.com password1")
        self.assertEqual(self.ui.command("Access Developer 02012"), 'NOT FOUND')

    def test_hire_valid_developer(self):
        # User Story : .....
        self.ui.command("Login employee1@company.com password1")
        self.assertEqual(self.ui.command("Hire 67891 employee2@company.com Developer"),
                         'Developer: <developer bro> hired by Scrum Manager : scrum bro')

    def test_hire_invalid_developer(self):
        # User Story : .....
        self.ui.command("Login employee1@company.com password1")
        self.assertEqual(self.ui.command("Hire 02012 employee23@company.com Developer"),
                         'Developer not found, and not exists in the company database')

    def test_add_developer_account(self):
        # User Story : .....
        self.ui.command("Login employee1@company.com password1")
        self.assertEqual(self.ui.command("Add Developer Account 67891 employee2@company.com"),
                         'Account for <developer bro> added successfully')

    def test_assign_developer_task(self):
        # User Story : .....
        self.ui.command("Login employee1@company.com password1")
        self.assertEqual(self.ui.command("Assign Task 12  Developer 67891 employee2@company.com"),
                         'Task Id: <12> Task Description: <Update the Terms Policy>'
                         'Task Estimate: <Medium> assigned to <developer bro> successfully')













