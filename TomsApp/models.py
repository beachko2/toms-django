from django.db import models


# Create your models here.
class Super(models.Model):
    name = models.CharField(max_length=20)


class Thingy(models.Model):
    stuff = models.CharField(max_length=20)
    superThing = models.ForeignKey(to=Super, on_delete=models.Cascade)


class YourClass:
    def command(self, inStr):
        return inStr