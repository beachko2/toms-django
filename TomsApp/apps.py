from django.apps import AppConfig


class TomsappConfig(AppConfig):
    name = 'TomsApp'
